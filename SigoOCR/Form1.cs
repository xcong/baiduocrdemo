﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SigoOCR
{
    public partial class Form1 : Form
    {
        Dictionary<string, int> data = new Dictionary<string, int>();
        List<string> pathList = new List<string>();

        // 设置APPID/AK/SK
        string APP_ID = "24435424";
        string API_KEY = "qSwsIDOkKcPPmKlo1g1sQNVa";
        string SECRET_KEY = "y2CMaspe367HGlUIGXqye7LmUtehthKc";



        public Form1()
        {
            InitializeComponent();

            pathList.Add(@"D:\360极速浏览器下载\河道排口拍照\外城河\");
            pathList.Add(@"D:\360极速浏览器下载\三公区雨水口挂牌照片（四季金辉-红旗河上游）\");
            pathList.Add(@"D:\360极速浏览器下载\一工区雨水口\雨水口0516\");

            data.Add("小南河", 41);
            data.Add("大寨河", 42);
            data.Add("洪福河", 43);
            data.Add("纬渠", 44);
            data.Add("经渠", 45);
            data.Add("清安河", 46);
            data.Add("红旗河", 47);
            data.Add("内城河", 48);
            data.Add("外城河", 49);
            data.Add("圩河", 50);
            data.Add("文渠河", 51);
            data.Add("科技大沟", 52);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var client = new Baidu.Aip.Ocr.Ocr(API_KEY, SECRET_KEY);
            client.Timeout = 60000;  // 修改超时时间

            foreach (var path in pathList)
            {
                DirectoryInfo root = new DirectoryInfo(path);
                FileInfo[] files = root.GetFiles();
                foreach (var filePath in files)
                {
                    var image = File.ReadAllBytes(filePath.FullName);
                    // 调用通用文字识别, 图片参数为本地图片，可能会抛出网络等异常，请使用try/catch捕获
                    var result = client.GeneralBasic(image);
                    Console.WriteLine(result);
                    // 如果有可选参数
                    var options = new Dictionary<string, object>{
        {"language_type", "CHN_ENG"},
        {"detect_direction", "true"},
        {"detect_language", "true"},
        {"probability", "true"}
    };
                    // 带参数调用通用文字识别, 图片参数为本地图片
                    result = client.GeneralBasic(image, options);
                    if (result != null)
                    {
                        JD dataJson = JsonConvert.DeserializeObject<JD>(JsonConvert.SerializeObject(result));
                        if (dataJson != null)
                        {
                            if (dataJson.words_result == null)
                            {
                                this.BeginInvoke(new EventHandler(delegate
                                {
                                    txtOCRText.Text += filePath.FullName + "\r\n";
                                }));
                            }
                            else
                            {
                                String join = String.Join("|", dataJson.words_result.Select(t => t.words).ToList());
                                this.BeginInvoke(new EventHandler(delegate
                                {
                                    txtOCRText.Text += join + "\r\n";
                                }));

                            }
                        }
                    }
                    Console.WriteLine(result);
                }
            }
        }
    }
}
